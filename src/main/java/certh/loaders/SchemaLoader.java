package certh.loaders;

import certh.exceptions.InvalidResourcePath;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class SchemaLoader {
    private static final Logger LOG = Logger.getLogger(SchemaLoader.class);
    private static final String SCHEMA_FILE_EXTENSION = ".avsc";
    private final Map<String, Schema> schemaMap = new HashMap<>();
    private final Schema.Parser parser = new Schema.Parser();
    private Path rootFolder;

    /**
     * Load AVRO schemas from the default resource location.
     *
     * @throws InvalidResourcePath
     */
    public void load() throws InvalidResourcePath {
        URL url = getClass().getClassLoader().getResource("avro");

        try {
            URI uri = url.toURI();
            LOG.debug(String.format("loading schemas from URI: %s", uri));
            try {
                FileSystems.newFileSystem(uri, new HashMap<>());
            } catch (Exception ignored) {
            }
            load(Paths.get(uri));
        } catch (NullPointerException | URISyntaxException | IOException e) {
            throw new InvalidResourcePath("failed to locate AVRO resources");
        }
    }

    /**
     * Load all AVRO schemas of a given folder.
     *
     * @param rootFolder root folder.
     * @throws IOException
     */
    public void load(final Path rootFolder) throws IOException {
        LOG.info(String.format("loading AVRO schemas from '%s'", rootFolder));
        this.rootFolder = rootFolder;

        computeSchemaFileList()
                .stream()
                .filter(path -> !loadBasicSchema(path))
                .collect(Collectors.toList())
                .forEach(this::loadSchema);
    }

    public GenericRecord createGenericRecord(final String schemaId) {
        return new GenericData.Record(getSchema(schemaId));
    }

    private Schema getSchema(final String schemaId) {
        return schemaMap.get(schemaId);
    }

    private String getFileId(final Path path) {
        return rootFolder.relativize(path).toString().replace(SCHEMA_FILE_EXTENSION, "");
    }

    private List<Path> computeSchemaFileList() throws IOException {
        return Files.walk(rootFolder)
                .filter(f -> f.toString().toLowerCase().endsWith(SCHEMA_FILE_EXTENSION))
                .collect(Collectors.toList());
    }

    private boolean loadSchema(final Path path) {
        try {
            Schema schema = parser.parse(path.toUri().toURL().openStream());
            String schemaId = getFileId(path);
            schemaMap.put(schemaId, schema);
            LOG.debug(String.format("loaded schema '%s' from '%s'", schemaId, path));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean loadBasicSchema(final Path path) {
        final Schema.Parser tempParser = new Schema.Parser();
        try {
            tempParser.parse(path.toUri().toURL().openStream());
            return loadSchema(path);
        } catch (Exception e) {
            return false;
        }
    }

    /*
    public static void main(String[] args) throws InvalidResourcePath {
        BasicConfigurator.configure();
        SchemaLoader loader = new SchemaLoader();
        loader.load();
    }*/
}
