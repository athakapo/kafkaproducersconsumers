package certh.resourceController;

import certh.events.KafkaReceiveEvent;
import certh.exceptions.InvalidResourcePath;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import org.apache.log4j.Logger;


public class Payload {

    private static final Logger LOG = Logger.getLogger(Payload.class);
    private EventBus eventBus;
    private String TestBedName;

    public Payload(EventBus eventBus, String TestBed) {
        this.eventBus = eventBus;
        this.TestBedName = TestBed;
    }

    @Subscribe
    public void receive(KafkaReceiveEvent event) throws InvalidResourcePath {

        LOG.info("New kafka message on topic" + event.getTopic() +" has been received.");
        //Handle kafka messages
        // ...
    }

}
