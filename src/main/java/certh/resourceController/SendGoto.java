package certh.resourceController;

import certh.exceptions.InvalidResourcePath;
import certh.loaders.SchemaLoader;
import com.google.common.eventbus.EventBus;
import org.apache.avro.generic.GenericRecord;
import org.apache.log4j.Logger;

import static certh.kafka.ProducerHelper.dispatch;
import static certh.kafka.ProducerHelper.setRecordHeader;

public class SendGoto {

    private static final Logger LOG = Logger.getLogger(Payload.class);
    private SchemaLoader allSchemas;
    private EventBus eventBus;

    public SendGoto(EventBus eventBus) throws InvalidResourcePath {
        this.eventBus = eventBus;
        //Load data structures
        allSchemas = new SchemaLoader();
        allSchemas.load();
    }

    public void send() {

        // Create a generic record with a predefined structure
        GenericRecord GoToType = allSchemas.createGenericRecord("goto");
        GenericRecord point = allSchemas.createGenericRecord("location");

        // Add values on the required fields of the nested schema (location)
        point.put("latitude", java.lang.Math.toRadians(40.613878));
        point.put("longitude", java.lang.Math.toRadians(22.972837));
        point.put("height", (float) 0.0);
        point.put("n", 0.0);
        point.put("e", 0.0);
        point.put("d", 0.0);
        point.put("depth", (float) 0.0);
        point.put("altitude", null);

        // Set up the message header
        setRecordHeader(GoToType);

        // Put it all together
        GoToType.put("timeout", 3600f);
        GoToType.put("location", point);
        GoToType.put("speed", 3.14f);

        //Load it!
        dispatch(GoToType, 0, "certh.convcao.dji.phantom4pro.hawk.1",
                "thessaloniki" + "_" + GoToType.getSchema().getName(), eventBus);

        LOG.info("Message has been dispatched to " + GoToType.getSchema().getName());
    }

}
