package certh.kafka;

import com.google.common.eventbus.EventBus;
import certh.events.KafkaDispatchEvent;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;

public class ProducerHelper {

    public static void setRecordHeader(GenericRecord record) {
        GenericRecord header = new GenericData.Record(record.getSchema().getField("header").schema());
        header.put("time", System.currentTimeMillis());
        header.put("sourceSystem", "Resource Controller");
        header.put("sourceModule", "Planner");
        record.put("header", header);
    }

    public static void dispatch(GenericRecord record, int partion_id, String name, String topicName,
                                EventBus eventBus){
        eventBus.post(new KafkaDispatchEvent(topicName, partion_id, name, record));
    }

}
