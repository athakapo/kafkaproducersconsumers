package certh.kafka;

import com.google.common.eventbus.EventBus;
import certh.events.KafkaReceiveEvent;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class Consumer {
    private static final Logger LOG = Logger.getLogger(Consumer.class);
    private static final int CONSUMER_THREADS = 4;
    private final Properties properties;
    private final List<String> topics;
    private final EventBus eventBus;
    private ExecutorService executor;
    private final List<ConsumerLoop> consumers;


    public Consumer(final Properties properties, final EventBus eventBus, final String topicFilter) {
        this.properties = properties;
        this.eventBus = eventBus;
        this.topics = Arrays.asList(topicFilter.split(Pattern.quote("|")));
        this.consumers = new ArrayList<>();
        eventBus.register(this);
    }

    public void start() {

        LOG.info("creating message streams");
        executor = Executors.newFixedThreadPool(CONSUMER_THREADS);
        for(int i = 0; i < CONSUMER_THREADS; i++) {
            ConsumerLoop consumer = new ConsumerLoop(i, properties, topics, eventBus);
            consumers.add(consumer);
            executor.submit(consumer);
        }
        LOG.info(String.format("created %d message streams", CONSUMER_THREADS));

    }

    public void shutdown() {
        LOG.info("shutting down");
        for (ConsumerLoop consumer : consumers) {
            consumer.shutdown();
        }
        executor.shutdown();
        try {
            if (!executor.awaitTermination(5000, TimeUnit.MILLISECONDS)) {
                LOG.error("timed out waiting for consumer threads to shut down, exiting uncleanly");
            }
        } catch (InterruptedException e) {
            LOG.error("interrupted during shutdown, exiting uncleanly");
        }
    }


    public class ConsumerLoop implements Runnable {
        private final KafkaConsumer<String, GenericRecord> consumer;
        private final List<String> topics;
        private final int id;
        private final EventBus eventBus;

        public ConsumerLoop(int id, Properties props, List<String> topics, EventBus eventBus) {
            this.eventBus = eventBus;
            this.id = id;
            this.topics = topics;
            this.consumer = new KafkaConsumer<>(props);
        }

        @Override
        public void run() {
            try {

                consumer.subscribe(topics);
                while (true) {
                    ConsumerRecords<String, GenericRecord> records = consumer.poll(Long.MAX_VALUE);
                    for (ConsumerRecord<String, GenericRecord> record : records) {
                        eventBus.post(new KafkaReceiveEvent(record.topic(), record.partition(), record.key(), record.value()));
                        //System.out.println(record.value());
                    }
                }
            } catch (WakeupException e) {
                // ignore for shutdown
            } finally {
                consumer.close();
            }
        }

        public void shutdown() {
            consumer.wakeup();
        }
    }

}
