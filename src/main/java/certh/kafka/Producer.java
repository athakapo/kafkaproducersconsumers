
package certh.kafka;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import certh.events.KafkaDispatchEvent;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.log4j.Logger;

import java.util.Properties;

public class Producer {
    private static final Logger LOG = Logger.getLogger(Producer.class);
    private final org.apache.kafka.clients.producer.Producer<String, GenericRecord> producer;

    public Producer(final Properties properties, final EventBus eventBus) {
        this.producer = new org.apache.kafka.clients.producer.KafkaProducer<>(properties);
        eventBus.register(this);
    }

    @Subscribe
    public void handleDispatchEvent(KafkaDispatchEvent event) {
        try {
            LOG.trace(String.format("dispatching %s", event.getTopic()));
            long startTime = System.currentTimeMillis();

            producer.send(new ProducerRecord<String, GenericRecord>(event.getTopic(),
                    event.getPartition(),
                    event.getKey(),
                    event.getRecord()));

            long elapsedTime = System.currentTimeMillis() - startTime;
            LOG.trace(String.format("dispatched %s in %d ms", event.getTopic(), elapsedTime));
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }
}
