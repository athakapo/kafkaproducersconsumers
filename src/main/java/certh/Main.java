package certh;

import certh.kafka.Consumer;
import certh.kafka.Producer;
import certh.resourceController.Payload;
import certh.resourceController.SendGoto;
import com.google.common.eventbus.EventBus;
import certh.exceptions.InvalidResourcePath;
import certh.loaders.ResourceLoader;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.IOException;
import java.util.Properties;

public class Main {
    private static final Logger LOG = Logger.getLogger(Main.class);
    private static final String PROPERTIES_LOG = "logging.properties";
    private static final String PROPERTIES_KAFKA = "kafka.properties";
    private static String KAFKA_TOPICS = "ExperimentStartRequest|ExperimentCancelRequest|ExperimentChangeRequest";

    public static void main(String[] args) throws IOException, InvalidResourcePath {
        EventBus eventBus = new EventBus();

        // Load properties
        LOG.info("starting the application");
        ResourceLoader resourceLoader = new ResourceLoader();
        PropertyConfigurator.configure(resourceLoader.getProperties(PROPERTIES_LOG));

        // Kafka
        LOG.info("loading kafka properties");
        Properties kafkaProperties = resourceLoader.getProperties(PROPERTIES_KAFKA);

        // Read from program arguments the kafka url and the testbed name
        String urlServer, TestBedCanonicalName;
        if (args.length == 2) {
            urlServer = args[0];
            TestBedCanonicalName = args[1];
        } else {
            System.err.format("Re-run the service with EXACTLY 2 arguments:\n");
            System.err.format("1) Kafka server url to connect\n");
            System.err.format("2) The canonical name of the current testbed\n");
            System.err.format("The service has stopped\n");
            return;
        }
        //***************** Setup kafka producer *****************
        //1. setup a unique group.id
        kafkaProperties.setProperty("group.id", TestBedCanonicalName + "_from_" + System.getProperty("user.name")
                + "_started_at_" + System.currentTimeMillis());
        //2. define bootstrap.servers by combining the kafka url and the default port
        kafkaProperties.setProperty("bootstrap.servers", urlServer + ":"+
                ((String) kafkaProperties.get("bootstrap.servers")).split(":")[1]);
        //3. define schema.registry.url
        kafkaProperties.setProperty("schema.registry.url", "http://" + urlServer + ":"+
                ((String) kafkaProperties.get("bootstrap.servers")).split(":")[1]);
        //4. Instantiate the producer
        final Producer kafkaProducer = new Producer(kafkaProperties, eventBus);
        //5. send a dummy message
        SendGoto msg = new SendGoto(eventBus);
        msg.send();

        //***************** Setup kafka consumer *****************
        //1. Define the topics that the consumer should be subscribed to
        KAFKA_TOPICS = KAFKA_TOPICS + "|" + TestBedCanonicalName + "_Location";
        //2. Create a class to handle received messages
        Payload PayloadInstance = new Payload(eventBus, TestBedCanonicalName);
        //3. Instantiate the consumer with all the previous defined properties
        Consumer kafkaConsumer = new Consumer(kafkaProperties, eventBus, KAFKA_TOPICS);
        //4. Start the consumer
        kafkaConsumer.start();
        //5. Register the consumer class to the event bus
        eventBus.register(PayloadInstance);



    }

}
